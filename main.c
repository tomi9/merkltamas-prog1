#include <stdio.h>
#include <stdlib.h>


void csere (int *n1, int *n2);
void process(int a, int b);
int primcheck(int n);
int n1, n2, i, p;

int main(int argc, char *argv[])  {
    if(argc==1) {
            printf("adjon meg ket kulonbozo szamot:");
            return 0;
    }
        else if(argc==2){
            printf("adjon meg egy masik szamot is:");
            return 0;
        }
        else if(argc>3) {
            printf("tul sok szamot adott meg");
            return 0;
        }
        else if(argc==3) {
        n1=atoi(argv[1]);
        n2=atoi(argv[2]);
        }

    csere(&n1, &n2);
    process(n1, n2);

    return 0;
}

void csere(int *n1, int *n2) {
    int swap;
    if(*n1>*n2) {
        swap=*n1;
        *n1=*n2;
        *n2=swap;
    }
    return;
}

void process(int a, int b) {
    if(n1>=0 && n2>=0) {
    printf("prim szamok %d es %d kozott: \n", n1, n2);
    for (i=n1+1; i<n2; ++i) {
        p=primcheck(i);
        if (p==1)
            printf("%d\n", i);
    }} else {
    printf ("csak pozitiv szamokat adhat meg");
    return;
    }
    return;
}

int primcheck(int n) {
    int j, p=1;
    for (j=2; j<=n/2; ++j){
        if(n%j==0) {
            p=0;
            break;
        }
    }
    return p;
}
